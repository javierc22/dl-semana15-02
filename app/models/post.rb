class Post < ApplicationRecord
  belongs_to :user

  # Con relación opcional
  # belongs_to :user, optional: true
end
