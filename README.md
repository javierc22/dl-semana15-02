# Semana 15 - Ejemplo 2: Devise

### Autenticaión manual

1. Intro a autenticación
2. Mi primera sesión
3. Setup del proyecto
4. Primer formulario de registro
5. Procesando el registro
6. Iniciando sesión con el usuario creado
7. El objeto current_user
8. Cierre de sesión
9. Formulario de ingreso
10. Procesando el ingreso
11. Bloqueando el acceso
12. Asignando User al Post
13. Intro a Bcrypt
14. Quitando password, agregando password_digest
15. Atributos virtuales y confirmaciones
16. Introducción a Bcrypt parte 2
17. Callback y encriptación automática
18. Comparando el password
19. Refactoring con has_secure_password
20. Reflexiones sobre controllers y modelos

### Autenticación con Devise

1. Intro, pros y contras de Devise
2. Pasos básicos para la instalación
3. Setup de un proyecto con Devise
4. Agregando Bootstrap, jquery y navbar
5. Ingreso registro y salida
6. Aunthenticate_user
7. Strong params con Devise
8. Current_user y relaciones opcionales